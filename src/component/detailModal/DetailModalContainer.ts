import { connect } from 'react-redux';
import { Reducer, Action } from 'redux';
import { BoxContent } from '../box/Box';
import { DetailModal } from './DetailModal';

const defaultState = {
    selectedBoxContent: {},
    visible: false,
};

const actionNames = {
    openModal: 'DetailModalContainer: openModal',
    closeModal: 'DetailModalContainer: closeModal',
};

interface FancyAction extends Action {
    payload?: BoxContent;
}

export const openModal = ( selectedBoxContend: BoxContent ): FancyAction => ({
    type: actionNames.openModal,
    payload: selectedBoxContend,
});

export const closeModal = (): FancyAction => ({
    type: actionNames.closeModal,
});

export const DetailModalReducer: Reducer<any> = ( state: any = defaultState, action: FancyAction ) => {
    switch ( action.type ) {
        case actionNames.openModal:
            return {
                visible: true,
                selectedBoxContent: action.payload,
            };
        case actionNames.closeModal:
            return defaultState;
        default:
            return state;
    }
};

const mapStateToProps = ( state: any, ownProps: any  ) => ({
    boxContent: state.detailModal.selectedBoxContent,
    visible: state.detailModal.visible,
});

const mapDispatchToProps = ( dispatch: any ) => ({
    onOk: () => dispatch( closeModal()),
});

export const DetailModalContainer = connect( 
    mapStateToProps,
    mapDispatchToProps,
)( DetailModal );