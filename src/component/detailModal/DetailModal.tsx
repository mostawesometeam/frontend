import * as React from 'react';
import { Modal } from 'antd';
import { BoxContent } from '../box/Box';

interface DetailModalProps {
    boxContent: BoxContent;
    visible: boolean;
    onOk: () => void;
}

export const DetailModal: React.SFC<DetailModalProps> = ({ 
    boxContent,
    visible,
    onOk,
}) => (
    <Modal
        title={boxContent.title}
        visible={visible}
        onOk={onOk}
        onCancel={onOk}
    >
        <h1>{boxContent.title}</h1>
        <p>{boxContent.description}</p>
        <br/>
        <p><b>Link:</b> <a target="_blank" href={boxContent.link} >{boxContent.link}</a></p>
    </Modal>
);
