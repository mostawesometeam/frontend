import { Action, Store } from 'redux';
import { Observable } from 'rxjs';

export enum BoxActionName {
    SetStatusArray = 'Box: setStatusArray',
    StartPolling = 'Box: startPolling',
    StopPoling = 'Box: stopPolling',
}

export const setStatusArray = ( payload: Array<any>) => ({
    type: BoxActionName.SetStatusArray,
    payload,
});

export const startPolling = () => ({
    type: BoxActionName.StartPolling,
});

export const BoxReducer = ( state: any = [], action: Action ) => {
    switch ( action.type ) {
        case BoxActionName.SetStatusArray: {
            return (action as any).payload;
        }
        default:
            return state;
    }
};

export const BoxEpic = (
    action$: Observable<Action>, 
    store: Store<any>): Observable<Action> => 
    action$
        .filter( action => action.type === BoxActionName.StartPolling )
        .switchMap( action =>
            Observable
                .timer( 0, 1000 )
                .retry()
                .switchMap( someAction => 
                    Observable
                        .ajax( 'http://localhost:8080/status' )
                        .map( payload =>  setStatusArray( payload.response ))
                )
        );