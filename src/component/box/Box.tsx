import * as React from 'react';
import './Box.css';

export interface BoxContent {
    title: string;
    status: string;
    description?: string;
    link?: string;
}

export interface BoxProps extends BoxContent {
    onClick: ( content: BoxContent ) => void;
}

export const Box: React.SFC<BoxProps> = ({ 
    title, 
    status, 
    description, 
    link,
    onClick,
}) => (
    <div 
        className="box-looks"
        style={{
            margin: '10px',
            borderColor: status,
            fontSize: '25px',
            textAlign: 'center',
            verticalAlign: 'middle',
        }}
        onClick={() => onClick({ title, status, description, link })}
    >
        {title}
    </div>
);