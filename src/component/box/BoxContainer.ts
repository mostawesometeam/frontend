import { connect } from 'react-redux';
import { Box, BoxContent } from './Box';
import { openModal } from '../detailModal/DetailModalContainer';

const mapStateToProps = (state: any, ownProps: BoxContent) => ({
    ...ownProps
});
const mapDispatchToProps = ( dispatch: any ) => ({
    onClick: ( content: BoxContent ) => dispatch( openModal( content )),
});

export const BoxContainer = connect( 
    mapStateToProps,
    mapDispatchToProps,
)( Box );