import * as React from 'react';
import { BoxContent } from './Box';
import { BoxContainer } from './BoxContainer';

export const renderBoxes = (boxInfos: Array<BoxContent>, className: string = 'box-container') =>
    boxInfos.map(boxInfo => (
        <BoxContainer
            key={boxInfo.title}
            {...boxInfo}
        />
    )
);
