import * as React from 'react';
import { connect } from 'react-redux';
import { renderBoxes } from '../box/BoxUtils';
import './OverView.css';

export const OverView: React.SFC<any> = ({ boxes }) => (
    <div className="over-view" >
        {
            ...renderBoxes( boxes )
        }
    </div>
);

export const mapStateToProps = (state: any) => ({
    boxes: state.box,
});

export default connect(
    mapStateToProps,
)( OverView );