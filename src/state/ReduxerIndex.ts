import { BoxReducer } from '../component/box/BoxER';
import { DetailModalReducer } from '../component/detailModal/DetailModalContainer';

export default {
    box: BoxReducer,
    detailModal: DetailModalReducer,
};