import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import { createEpicMiddleware, combineEpics } from 'redux-observable';
import epics from './EpicIndex';
import reducers from './ReduxerIndex';
import { startPolling as start } from '../component/box/BoxER';

const rxMiddleware = createEpicMiddleware( combineEpics( ...epics ));
const  reducer = combineReducers({ ...reducers });

const store = createStore(
    reducer,
    compose(
        applyMiddleware(
            rxMiddleware
        ),
        (window as any).__REDUX_DEVTOOLS_EXTENSION__ && (window as any).__REDUX_DEVTOOLS_EXTENSION__()
    )
);

export const startPolling = () => store.dispatch(start());
export const getStore = () => store;
  