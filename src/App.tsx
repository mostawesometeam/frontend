import * as React from 'react';
import OverView from './component/overview/OverView';
import { DetailModalContainer } from './component/detailModal/DetailModalContainer';
import { startPolling } from './state/store';

const StartPolling: React.SFC<any> = (props) => {
  startPolling();
  return null;
};

const App = () => (
  <div>
    <StartPolling/>
    <OverView/>
    <DetailModalContainer />
  </div>
);

export default App;
