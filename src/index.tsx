import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import App from './App';
import { getStore } from './state/store';

import registerServiceWorker from './registerServiceWorker';
import './index.css';
import 'antd/dist/antd.css';
import 'rxjs';

const store = getStore();

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
